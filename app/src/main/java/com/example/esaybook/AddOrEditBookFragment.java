package com.example.esaybook;

import static java.lang.Integer.parseInt;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.MenuProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Lifecycle;
import androidx.navigation.Navigation;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.example.esaybook.databinding.FragmentAddOrEditBookBinding;
import com.example.esaybook.model.Book;
import com.example.esaybook.model.Model;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;
import java.util.Objects;

public class AddOrEditBookFragment extends Fragment {
    private FragmentAddOrEditBookBinding binding;
    private ActivityResultLauncher<Void> cameraLauncher;
    private ActivityResultLauncher<String> galleryLauncher;
    private Boolean isImageSelected = false;

    public AddOrEditBookFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentActivity parentActivity = getActivity();

        parentActivity.addMenuProvider(new MenuProvider() {
            @Override
            public void onCreateMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
                menu.removeItem(R.id.addBookFragment);
            }

            @Override
            public boolean onMenuItemSelected(@NonNull MenuItem menuItem) {
                return false;
            }
        },this, Lifecycle.State.RESUMED);

        cameraLauncher = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), new ActivityResultCallback<Bitmap>() {
            @Override
            public void onActivityResult(Bitmap result) {
                if (result != null) {
                    binding.bookImg.setImageBitmap(result);
                    isImageSelected = true;
                }
            }
        });

        galleryLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
            @Override
            public void onActivityResult(Uri result) {
                if (result != null){
                    binding.bookImg.setImageURI(result);
                    isImageSelected = true;
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentAddOrEditBookBinding.inflate(inflater,container,false);
        View view = binding.getRoot();

        setFields();
        setSaveButtonActionListener();
        setCancelButtonActionListener();
        setCameraButtonActionListener();
        setGalleryButtonActionListener();

        return view;
    }

    private void setFields() {
        Bundle bundle = getArguments();

        if (bundle.get("book") != null) {
            Book book = (Book) bundle.get("book");
            binding.nameEt.setText(book.getName());
            binding.authorEt.setText(book.getAuthor());
            binding.categoryEt.setText(book.getCategory());
            binding.priceEt.setText("" + book.getPrice() + "");
            binding.rateEt.setText("" + book.getRate() + "");

            if (!Objects.equals(book.getImageUrl(), ""))
            {
                Picasso.get().load(book.getImageUrl()).into(binding.bookImg);
            }
        }
    }

    private void setGalleryButtonActionListener() {
        binding.galleryButton.setOnClickListener(view1->{
            galleryLauncher.launch("image/*");
        });
    }

    private void setCameraButtonActionListener() {
        binding.cameraButton.setOnClickListener(view1->{
            cameraLauncher.launch(null);
        });
    }

    private void setCancelButtonActionListener() {
        binding.cancelBtn.setOnClickListener(view1 -> Navigation.findNavController(view1).popBackStack(R.id.myPostsFragment,false));
    }

    private void setSaveButtonActionListener() {
        binding.saveBtn.setOnClickListener(view1 -> {
            String name = binding.nameEt.getText().toString();
            String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            String author = binding.authorEt.getText().toString();
            String category = binding.categoryEt.getText().toString();
            String summary = "Summary Unavailable";
            String imageUrl = "";
            int price = parseInt(binding.priceEt.getText().toString());
            int rate = parseInt(binding.rateEt.getText().toString());

            Book bookToSave = new Book(name, userId, author, category, summary, imageUrl, price, rate);

            BookSummaryApi.instance().getBookSummaryByName(name, bookSummary -> {
                bookToSave.setSummary(bookSummary);
            });

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Bundle bundle = getArguments();
            if (bundle.get("book") != null) {
                Book book = (Book) bundle.get("book");
                bookToSave.setImageUrl(book.getImageUrl());
            }

            if (isImageSelected) {
                binding.bookImg.setDrawingCacheEnabled(true);
                binding.bookImg.buildDrawingCache();
                Bitmap bitmap = ((BitmapDrawable) binding.bookImg.getDrawable()).getBitmap();
                Model.instance().uploadImage(name, bitmap, url->{
                    if (url != null){
                        bookToSave.setImageUrl(url);
                    }
                    Model.instance().addBook(bookToSave, (unused) -> {
                        Navigation.findNavController(view1).popBackStack();
                    });
                });
            } else {
                Model.instance().addBook(bookToSave, (unused) -> {
                    Navigation.findNavController(view1).popBackStack();
                });
            }
        });
    }
}