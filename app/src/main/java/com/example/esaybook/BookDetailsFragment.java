package com.example.esaybook;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.esaybook.databinding.FragmentBookDetailsBinding;
import com.example.esaybook.model.Book;
import com.example.esaybook.model.FirebaseModel;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;
import java.util.Objects;

public class BookDetailsFragment extends Fragment {
    FragmentBookDetailsBinding binding;

    public BookDetailsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentBookDetailsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        setFields();

        return view;
    }

    private void setFields() {
        Bundle bundle = getArguments();

        if (bundle != null) {
            Book book = (Book) bundle.get("book");
            binding.name.setText(book.getName());
            binding.author.setText(book.getAuthor());
            binding.category.setText(book.getCategory());
            binding.price.setText(book.getPrice() + "$");
            binding.rate.setText(book.getRate() + "*");
            if (!Objects.equals(book.getSummary(), "")) {
                binding.summary.setText(book.getSummary());
            }

            if (book.getPhoto() != null) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(book.getPhoto(), 0, book.getPhoto().length);
                binding.bookImage.setImageBitmap(bitmap);
            }
            else if (!Objects.equals(book.getImageUrl(), ""))
            {
                Picasso.get().load(book.getImageUrl()).into(binding.bookImage);
            }

            FirebaseModel firebaseModel = new FirebaseModel();
            firebaseModel.getUserById(book.getUserId(), user -> {
                binding.sellerFullName.setText("Full Name:  " + user.getFullName());
                binding.sellerPhoneNumber.setText("Phone Number:  " + user.getPhoneNumber());
                binding.sellerMail.setText("Mail:  " + user.getEmail());
            });
        }
    }
}