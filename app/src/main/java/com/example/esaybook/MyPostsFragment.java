package com.example.esaybook;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.esaybook.databinding.FragmentMyPostsBinding;
import com.example.esaybook.model.Model;

public class MyPostsFragment extends Fragment {
    FragmentMyPostsBinding binding;
    BookRecyclerAdapter adapter;
    MyPostsFragmentViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentMyPostsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        binding.myPostsRecyclerView.setHasFixedSize(true);
        binding.myPostsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new BookRecyclerAdapter(getLayoutInflater(), viewModel.getData().getValue(), "MyPosts");
        binding.myPostsRecyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new BookRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
            }
        });

        View addButton = view.findViewById(R.id.addBtn);
        NavDirections action = MyPostsFragmentDirections.actionMyPostsFragmentToAddBookFragment(null, "Add Book");
        addButton.setOnClickListener(Navigation.createNavigateOnClickListener(action));

        binding.myPostsProgressBar.setVisibility(View.GONE);

        viewModel.getData().observe(getViewLifecycleOwner(),list->{
            adapter.setData(list);
        });

        Model.instance().EventBooksListLoadingState.observe(getViewLifecycleOwner(), status->{
            binding.myPostsSwipeRefresh.setRefreshing(status == Model.LoadingState.LOADING);
        });

        binding.myPostsSwipeRefresh.setOnRefreshListener(()->{
            reloadData();
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        viewModel = new ViewModelProvider(this).get(MyPostsFragmentViewModel.class);
    }

    void reloadData() {
        Model.instance().refreshAllBooks();
    }
}