package com.example.esaybook;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import com.example.esaybook.databinding.BookListRowBinding;
import com.example.esaybook.model.Book;
import com.example.esaybook.model.FirebaseModel;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;
import java.util.List;
import java.util.Objects;

class BookViewHolder extends RecyclerView.ViewHolder {

    BookListRowBinding binding;
    TextView categoryTv;
    TextView bookNameTv;
    TextView authorTv;
    ImageView bookImage;
    List<Book> data;
    Button editButton;

    public BookViewHolder(@NonNull View itemView, BookRecyclerAdapter.OnItemClickListener listener, List<Book> data) {
        super(itemView);
        this.binding = BookListRowBinding.bind(itemView);;
        this.data = data;
        categoryTv = binding.category;
        bookNameTv = binding.bookName;
        authorTv = binding.author;
        bookImage = binding.bookImage;
        editButton = binding.editButton;

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = getAdapterPosition();
                listener.onItemClick(pos);
            }
        });
    }

    public void bind(Book book, String sourceFrag) {
        categoryTv.setText(book.getCategory());
        bookNameTv.setText(book.getName());
        authorTv.setText(book.getAuthor());

        if (book.getPhoto() != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(book.getPhoto(), 0, book.getPhoto().length);
            binding.bookImage.setImageBitmap(bitmap);
        } else if (!Objects.equals(book.getImageUrl(), "")) {
            Picasso.get().load(book.getImageUrl()).into(bookImage);
        }

        FirebaseModel firebaseModel = new FirebaseModel();
        firebaseModel.getUserById(FirebaseAuth.getInstance().getCurrentUser().getUid(), user -> {

            if(user.getId().equals(book.getUserId())) {
                editButton.setVisibility(View.VISIBLE);

                if (Objects.equals(sourceFrag, "MyPosts")) {
                    NavDirections action1 = MyPostsFragmentDirections.actionMyPostsFragmentToAddBookFragment(book, "Edit Book");
                    editButton.setOnClickListener(Navigation.createNavigateOnClickListener(action1));
                } else {
                    NavDirections action2 = BooksListFragmentDirections.actionBooksListFragmentToAddBookFragment(book, "Edit Book");
                    editButton.setOnClickListener(Navigation.createNavigateOnClickListener(action2));
                }
            }
        });
    }
}

public class BookRecyclerAdapter extends RecyclerView.Adapter<BookViewHolder> {

    OnItemClickListener listener;
    LayoutInflater inflater;
    List<Book> data;
    String sourceFrag;

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }

    public void setData(List<Book> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public BookRecyclerAdapter(LayoutInflater inflater, List<Book> data, String sourceFrag) {
        this.inflater = inflater;
        this.data = data;
        this.sourceFrag = sourceFrag;
    }

    void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.book_list_row, parent,false);
        return new BookViewHolder(view, listener, data);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        Book book = data.get(position);
        holder.bind(book, sourceFrag);
    }

    @Override
    public int getItemCount() {
        if (data == null) {
            return 0;
        }

        return data.size();
    }
}

