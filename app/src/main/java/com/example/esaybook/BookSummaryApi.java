package com.example.esaybook;

import com.example.esaybook.model.Model;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class BookSummaryApi {
    private static final BookSummaryApi instance = new BookSummaryApi();
    private OkHttpClient client;
    private String summary = "";

    private BookSummaryApi() {
        client = new OkHttpClient();
    }

    public static BookSummaryApi instance() {
        return instance;
    }

    public void getBookSummaryByName(String name, Model.Listener<String> callback) {
        Request request = new Request.Builder()
                .url("https://api.nytimes.com/svc/books/v3/reviews.json?title=" + name.replaceAll(" ", "+") +
                        "&api-key=XI30pZWIs2xYpGwKBBb4mN2Lajmj4Fw0")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        JSONObject json = new JSONObject(response.body().string());
                        JSONArray results = json.getJSONArray("results");
                        for (int i = 0; i < results.length(); i++) {
                            JSONObject result = results.getJSONObject(i);
                            summary = result.getString("summary");
                            callback.onComplete(summary);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
