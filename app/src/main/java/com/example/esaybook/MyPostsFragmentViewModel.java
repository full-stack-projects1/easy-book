package com.example.esaybook;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.example.esaybook.model.Book;
import com.example.esaybook.model.Model;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import java.util.List;

public class MyPostsFragmentViewModel extends ViewModel {

    LiveData<List<Book>> getData() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            return Model.instance().getAllBooksByUserId(user.getUid());
        }

        return null;
    }
}
