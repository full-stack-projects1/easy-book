package com.example.esaybook;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.example.esaybook.databinding.FragmentBooksListBinding;
import com.example.esaybook.model.Book;
import com.example.esaybook.model.Model;

public class BooksListFragment extends Fragment {
    FragmentBooksListBinding binding;
    BookRecyclerAdapter adapter;
    BooksListFragmentViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentBooksListBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new BookRecyclerAdapter(getLayoutInflater(), viewModel.getData().getValue(), "BooksList");
        binding.recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new BookRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                Log.d("TAG", "Row was clicked " + pos);
                Book book = viewModel.getData().getValue().get(pos);
                BooksListFragmentDirections.ActionBooksListFragmentToBookDetailsFragment action =
                        BooksListFragmentDirections.actionBooksListFragmentToBookDetailsFragment(book);
                Navigation.findNavController(view).navigate(action);
            }
        });

        binding.progressBar.setVisibility(View.GONE);

        viewModel.getData().observe(getViewLifecycleOwner(),list-> {
            adapter.setData(list);
        });

        Model.instance().EventBooksListLoadingState.observe(getViewLifecycleOwner(), status-> {
            binding.swipeRefresh.setRefreshing(status == Model.LoadingState.LOADING);
        });

        binding.swipeRefresh.setOnRefreshListener(()-> {
            reloadData();
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        viewModel = new ViewModelProvider(this).get(BooksListFragmentViewModel.class);
    }

    void reloadData() {
        Model.instance().refreshAllBooks();
    }
}