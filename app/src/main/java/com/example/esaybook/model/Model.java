package com.example.esaybook.model;

import android.graphics.Bitmap;
import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Model {
    final public MutableLiveData<LoadingState> EventBooksListLoadingState = new MutableLiveData<LoadingState>(LoadingState.NOT_LOADING);
    private static final Model _instance = new Model();
    private LiveData<List<Book>> bookList;
    private LiveData<List<Book>> userBookList;
    private Executor executor = Executors.newSingleThreadExecutor();
    private FirebaseModel firebaseModel = new FirebaseModel();
    AppLocalDbRepository localDb = AppLocalDb.getAppDb();

    private Model() {}

    public static Model instance() {
        return _instance;
    }

    public interface Listener<T> {
        void onComplete(T data);
    }

    public enum LoadingState {
        LOADING,
        NOT_LOADING
    }

    public LiveData<List<Book>> getAllBooks() {
        if (bookList == null) {
            bookList = (LiveData<List<Book>>) localDb.bookDao().getAll();
            refreshAllBooks();
        }
        return bookList;
    }

    public LiveData<List<Book>> getAllBooksByUserId(String userId) {
        userBookList = (LiveData<List<Book>>) localDb.bookDao().getAllByUserId(userId);
        refreshAllBooks();

        return userBookList;
    }

    public void refreshAllBooks(){
        EventBooksListLoadingState.setValue(LoadingState.LOADING);
        Long localLastUpdate = Book.getLocalLastUpdate();
        firebaseModel.getAllBooksSince(localLastUpdate,list->{
            executor.execute(()->{
                Log.d("TAG", " firebase return : " + list.size());
                Long time = localLastUpdate;
                for(Book book: list){
                    book.setPhoto(urlToByteArr(book.getImageUrl()));
                    // insert new records into ROOM
                    localDb.bookDao().insertAll(book);
                    if (time < book.getLastUpdated()){
                        time = book.getLastUpdated();
                    }
                }
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Book.setLocalLastUpdate(time);
                EventBooksListLoadingState.postValue(LoadingState.NOT_LOADING);
            });
        });
    }

    public void addBook(Book book, Listener<Void> listener){
        firebaseModel.addBook(book, (Void)->{
            refreshAllBooks();
            listener.onComplete(null);
        });
    }

    public void addUser(User user, Listener<Void> listener){
        firebaseModel.addUser(user, (Void)-> {
            listener.onComplete(null);
        });
    }

    public void uploadImage(String name, Bitmap bitmap,Listener<String> listener) {
        firebaseModel.uploadImage(name,bitmap,listener);
    }

    public byte[] urlToByteArr(String link) {
        byte[] imageBytes = null;
        try {
            URL url = new URL(link);
            InputStream inputStream = url.openStream();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int length;

            while ((length = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
            }

            imageBytes = outputStream.toByteArray();
            inputStream.close();
            outputStream.close();

        } catch(Exception e) {
            System.out.println(e);
        } finally {
            return imageBytes;
        }
    }
}
