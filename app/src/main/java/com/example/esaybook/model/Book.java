package com.example.esaybook.model;

import static java.lang.Integer.parseInt;
import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import com.example.esaybook.MyApplication;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.FieldValue;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Entity
public class Book implements Serializable {
    private static final String NAME = "name";
    private static final String AUTHOR = "author";
    private static final String CATEGORY = "category";
    private static final String SUMMARY = "summary";
    private static final String IMAGE_URL = "imageUrl";
    private static final String PRICE = "price";
    private static final String RATE = "rate";
    private static final String USER_ID = "userId";
    public static final String COLLECTION = "books";
    static final String LAST_UPDATED = "lastUpdated";
    static final String LOCAL_LAST_UPDATED = "books_local_last_update";

    @PrimaryKey
    @NonNull
    private String name;
    private String userId;
    private String author;
    private String category;
    private String summary;
    private String imageUrl;
    private int price;
    private int rate;
    public Long lastUpdated;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    public byte[] photo;

    public Book() {}

    public Book(@NonNull String name, String userId, String author, String category, String summary, String imageUrl, int price, int rate) {
        this.name = name;
        this.userId = userId;
        this.author = author;
        this.category = category;
        this.summary = summary;
        this.imageUrl = imageUrl;
        this.price = price;
        this.rate = rate;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public static Book fromJson(Map<String, Object> json) {
        String name = (String)json.get(NAME);
        String userId = (String)json.get(USER_ID);
        String author = (String)json.get(AUTHOR);
        String category = (String)json.get(CATEGORY);
        String summary = (String)json.get(SUMMARY);
        String imageUrl = (String)json.get(IMAGE_URL);
        int price = parseInt(json.get(PRICE).toString());
        int rate = parseInt(json.get(RATE).toString());

        Book book = new Book(name, userId, author, category, summary, imageUrl, price, rate);

        try {
            Timestamp time = (Timestamp) json.get(LAST_UPDATED);
            book.setLastUpdated(time.getSeconds());
        } catch(Exception e) {}

        return book;
    }

    public static Long getLocalLastUpdate() {
        SharedPreferences sharedPref = MyApplication.getMyContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        return sharedPref.getLong(LOCAL_LAST_UPDATED, 0);
    }

    public static void setLocalLastUpdate(Long time) {
        SharedPreferences sharedPref = MyApplication.getMyContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(LOCAL_LAST_UPDATED,time);
        editor.commit();
    }

    public Map<String,Object> toJson() {
        Map<String, Object> json = new HashMap<>();

        json.put(NAME, getName());
        json.put(USER_ID, getUserId());
        json.put(AUTHOR, getAuthor());
        json.put(CATEGORY, getCategory());
        json.put(SUMMARY, getSummary());
        json.put(IMAGE_URL, getImageUrl());
        json.put(PRICE, getPrice());
        json.put(RATE, getRate());
        json.put(LAST_UPDATED, FieldValue.serverTimestamp());

        return json;
    }
}
