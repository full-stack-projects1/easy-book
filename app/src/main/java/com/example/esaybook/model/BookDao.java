package com.example.esaybook.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import java.util.List;

@Dao
public interface BookDao {
    @Query("select * from Book")
    LiveData<List<Book>> getAll();

    @Query("select * from Book where userId = :userId")
    LiveData<List<Book>> getAllByUserId(String userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Book... books);
}

