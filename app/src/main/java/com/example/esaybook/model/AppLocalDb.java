package com.example.esaybook.model;

import androidx.room.Room;
import com.example.esaybook.MyApplication;

public class AppLocalDb {
    private AppLocalDb() {}

    static public AppLocalDbRepository getAppDb() {
        return Room.databaseBuilder(MyApplication.getMyContext(),
                        AppLocalDbRepository.class,
                        "books.db")
                .fallbackToDestructiveMigration()
                .build();
    }
}

