package com.example.esaybook.model;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Book.class}, version = 5)
public abstract class AppLocalDbRepository extends RoomDatabase {
    public abstract BookDao bookDao();
}
