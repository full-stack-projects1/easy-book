package com.example.esaybook;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.example.esaybook.model.Book;
import com.example.esaybook.model.Model;
import java.util.List;

public class BooksListFragmentViewModel extends ViewModel {
    private LiveData<List<Book>> data = Model.instance().getAllBooks();

    LiveData<List<Book>> getData() {
        return data;
    }
}
